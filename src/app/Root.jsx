import React from 'react'
import {
  BrowserRouter, Switch, Route, Redirect,
} from 'react-router-dom'

import Home from './pages/Home'
import ErrorPage from './pages/ErrorPage'

const Root = () => (
  <BrowserRouter>
    <Switch>
      <Route exact path="/" component={Home} />

      <Route path="/error" component={ErrorPage} />
      <Redirect to="/error" />
    </Switch>
  </BrowserRouter>

)

export default Root
