export const setItem = (key, value) => {
  window.localStorage.setItem(key, value)
}

export const getItem = (key) => {
  const value = window.localStorage.getItem(key)
  return value === null ? '' : value
}

export const setToken = (value) => {
  setItem(process.env.USER_TOKEN, value)
}

export const clearToken = () => setToken('')

export const getToken = () => getItem(process.env.USER_TOKEN)
