### Start

```
cp .env.example .env
npm install
npm start
```

### Build development

```
npm run build:dev
```

### Build production

```
npm run build:prod
```

### Lint

```
npm run eslint
```
