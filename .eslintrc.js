module.exports = {
  extends: ['airbnb'],
  plugins: ['react', 'react-hooks'],
  rules: {
    'react/destructuring-assignment': 0,
    'react/forbid-prop-types': 0,
    'react/jsx-one-expression-per-line': 0,
    'react/jsx-props-no-spreading': 0,
    'react-hooks/rules-of-hooks': 'error',
    'react-hooks/exhaustive-deps': 'warn',
    'no-script-url': 0,
    'jsx-a11y/anchor-is-valid': 0,
    'jsx-a11y/label-has-for': 0,
    'jsx-a11y/label-has-associated-control': 0,
    'no-console': 2,
    semi: [2, 'never'],
    'semi-spacing': [2, {
      before: false,
      after: true,
    }],
  },
  globals: {
    document: true,
    window: true,
  },
  env: {
    browser: true,
    node: true,
    es6: true,
  },
}
